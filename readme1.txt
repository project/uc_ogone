�Ogone PSP payment module for Ubercart

PREREQUISITES

- Drupal 6.X

INSTALLATION

Drupal:
Install and activate this module like every other Drupal
module.

Ogone backend:
-Enable 'Get' under 1.1
-Fill in 'http://www.yourdomain.com/cart/checkout' under 2.2
-Fill in 'http://www.yourdomain.com/cart/ogone_return_ok' under 4.1 (OK URL)
-Enable "Make this request just after..." onder 4.2
-Enable "Display a ticket to the client if ..." under 5.2
-You have to use SHA-1 signatures, or else your merchant-feedback might be messed with.
 This module will NOT work without them. See SHA-1 signatures as pre-shared passwords if you do not know what it is.
  -3.2: must be the same as "SHA-1 Signature pre"
  -4.4: must be the same as "SHA-1 Signature post"
   (you can use the same key for both if you like)

FEATURES

- Payment method choice after redirection to Ogone
- Based on 'basic' integration handbook with some tweaks from the 'advanced' integration
- All settings adjustable in admin form


INFORMATION
Need a test account?
Get it here:
http://www.ogone.com/ncol/web_new_testaccount8.asp
(Choose e-commerce)
Ogone will review your data and send you an email with confirmation and a password.
You can login here: https://secure.ogone.com/ncol/test/frame_ogone.asp

New to Ogone? Check Ogone_e-Com-BAS_v1-1_EN.pdf, available to you after making a test account.

TIPS
-You could change the 'submit order' button text to something more meaningful like 'Go to Payment Screen'.
-It's tricky to test from localhost. You have to open up port 80 to that machine, and configure the right URL to it under 4.1.
-The return URL's are static. Please force visitors to use or not use the www. domain prefix, NEVER use both. This can cause loss of session (browser dependant) when a user comes from http://tld and returns after payment to http://www.tld.

THANKS
Thanks to the helpful Ubercart team and forum users for tips and answers.

DEVELOPMENT
This module is developed, maintained and distributed bij Qrios. We can be contracted for Drupal/Ubercart projects or (payment) module building. Mail us: info {at} qrios {dot} nl.

www.qrios.nl